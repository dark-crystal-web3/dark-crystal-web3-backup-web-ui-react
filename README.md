# Next.js / react version of dark-crystal-web3-backup web interface

This is for use by the secret owner and can be self-hosted and offline-first.

The components are in a separate module: [dark-crystal-web3-ui-components](https://gitlab.com/dark-crystal-web3/dark-crystal-web3-ui-components)

- Install `yarn`
- Dev server: `yarn run dev`
- Build `yarn run build`
- Deployed to: [https://web3-backup.darkcrystal.pw/](https://web3-backup.darkcrystal.pw/)
