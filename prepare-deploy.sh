#!/bin/sh
# hacky fix for: https://github.com/vercel/next.js/issues/25852
mkdir -p .next/server/chunks/static/wasm \
&& mkdir -p .next/server/static \
&& cd .next/server/static \
&& ln -s ../chunks/static/wasm wasm
