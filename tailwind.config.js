/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './node_modules/dark-crystal-web3-ui-components/components/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      colors: {
        'very-pale-pink': '#D7B8C0', // so-bg
        'very-dark-pink': '#4F1C35', // so-text
        'pale-pink': '#CAA0AF', // so-bubble-bg
        'dark-pink': '#821A4E',
      },
      fontFamily: {
        'silkscreen': ['SILKSCREEN'],
      },
      backgroundImage: {
        'crystal-background': "url('../img/cavepixelart.jpg')",
      },
      spacing: {
        'box': '0.625rem'
      }
    },
  },
  plugins: [],
}
