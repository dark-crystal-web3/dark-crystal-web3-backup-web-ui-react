const withTM = require('next-transpile-modules')(['dark-crystal-web3-ui-components']);

/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  cleanDistDir: false,
  assetPrefix: process.env.NODE_ENV === 'production' ? 'https://web3-backup.darkcrystal.pw' : undefined,
  webpack (config) {
    config.experiments = { asyncWebAssembly: true, layers: true }
    return config
  },
}

module.exports = withTM(nextConfig);
// swcMinify: true,
