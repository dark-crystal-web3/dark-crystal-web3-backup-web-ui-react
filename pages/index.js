import Head from 'next/head'
import React from 'react'
import * as dc from '../shared/share-recover.js'
import {
  AppTitle,
  Share,
  ShareSuccess,
  PageHeading,
  Recover,
  Button,
  icons,
  text,
} from 'dark-crystal-web3-ui-components'
import h from 'react-hyperscript'

// Test mode displays 'add test data' buttons and uses a dummy eth address
const testMode = true

// In the case of the online version, this would be the actual eth Address
// from the web3 interface
const ethAddress = undefined
// const ethAddress = testMode
//   ? '0xSomeEthAddressd76467d3348077d003f00ffb97'
//   : undefined

export default class Home extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      currentTab: 'backup',
    }
  }

  render() {
    const backupRecoverIcons = {
      backup: 'cofre-cerrado',
      recover: 'cofre-abierto',
    }

    return h('div', [
      h(Head, [
        h('title', 'Dark Crystal Web3'),
        h('link', { rel: 'icon', href: '/favicon.ico' }),
      ]),
      h('main', { className: 'pb-4' }, [
        h(
          'div',
          {
            className:
              'mx-auto max-w-screen-xxxl bg-crystal-background bg-center bg-cover text-white pb-4 mb-4',
          },
          [
            h(
              'div',
              { className: 'bg-gradient-to-b from-dark-pink px-4 pb-4' },
              [
                h('div', { className: 'flex flex-col md:flex-row justify-between pt-4' }, [
                  h(AppTitle),
                  h('div', [
                    h(
                      'div',
                      { className: 'flex flex-row' }, [
                        h('span', { className: 'hidden md:block mr-4 font-silkscreen uppercase text-xl mt-1' }, text.secretOwner),
                        h('span',
                          ['backup', 'recover'].map((tab) => {
                            return h('span', { className: 'mr-4' }, [
                              h(Button, {
                                key: tab,
                                onClick: () => {
                                  this.setState({ currentTab: tab })
                                },
                                active: this.state.currentTab === tab,
                                text: tab,
                                icon: icons(backupRecoverIcons[tab]),
                              })
                            ])
                          })
                        )
                      ]
                    ),
                  ]),
                ]),
              ]
            ),
            h(
              'div',
              {
                className: 'container mx-auto p-5 sm:px-10 lg:pl-36 my-auto',
              },
              [
                h(
                  PageHeading,
                  Object.assign(
                    {
                      icon: icons(backupRecoverIcons[this.state.currentTab]),
                    },
                    text.pageHeadings[this.state.currentTab]
                  )
                ),
              ]
            ),
          ]
        ),
        h('div', { className: 'container mx-auto h-80' }, [
          h('div', { className: 'p-5 sm:px-10 flex lg:pl-36 max-w-6xl' }, [
            h(Share, {
              ethAddress,
              visible: this.state.currentTab === 'backup',
              ShareSuccess,
              dc,
              testMode,
            }),
            h(Recover, {
              ethAddress,
              visible: this.state.currentTab === 'recover',
              dc,
              testMode,
            }),
          ]),
        ]),
      ]),
    ])
  }
}
